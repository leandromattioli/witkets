.. witkets documentation master file, created by
   sphinx-quickstart on Sat Jan 10 17:55:46 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

***************************
witkets: Tkinter extensions
***************************

Contents:
=========

.. toctree::
   :maxdepth: 2
   :numbered:
   
   intro
   tkbuilder
   theme
   widgets
   utilities

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

