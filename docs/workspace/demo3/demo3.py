import tkinter as tk
import tkinter.ttk as ttk
import witkets as wtk

root = tk.Tk()
s = wtk.Style()
wtk.Style.set_default_fonts()
s.apply_default()                  # applying default theme first
s.theme_use('clam')
s.apply_from_file('styles3b.ini')  # override buttons and create style class

ttk.Button(root, text='normal').pack(side=tk.LEFT)
b = ttk.Button(root, text='BIG One!')
b['style'] = 'BigButton.TButton'
b.pack(side=tk.LEFT, expand='1')

root.mainloop()