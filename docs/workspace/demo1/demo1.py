#!/usr/bin/env python3
import witkets as wtk

class App(wtk.Application):
    def __init__(self):
        wtk.Application.__init__(self, ui_filename='app.xml', title='Demo', 
                                 theme='clam')
        self.root['borderwidth'] = 10    
    # ... user methods for configuring and handling events
    # using self.root (main window) and self.builder (TkBuilder instance)

app = App()
app.run()