.. _new-widgets:

***********
New Widgets
***********

AccelLabel
==========

.. figure:: img/accellabel_screenshot.png
   :scale: 100
   :align: right

.. automodule:: witkets.accellabel
    :members:
  
CardLayout
==========

.. automodule:: witkets.cardlayout
   :members:

ColorButton
===========

.. figure:: img/colorbutton_screenshot.png
   :scale: 100
   :align: right

.. automodule:: witkets.colorbutton
    :members:

ConsoleView
===========

.. figure:: img/consoleview_screenshot.png
   :scale: 100
   :align: right

.. automodule:: witkets.consoleview
   :members:

Expander
========

.. figure:: img/expander_screenshot.png

.. automodule:: witkets.expander
   :members:


FileChooserEntry
================

.. figure:: img/filechooserentry_screenshot.png
   :scale: 100
   :align: right

.. automodule:: witkets.filechooserentry
    :members:

Gauge
=====

.. figure:: img/gauge_screenshot.png
   :scale: 100
   :align: right

.. automodule:: witkets.gauge
   :members:

ImageButton
===========

.. figure:: img/imagebutton_screenshot.png
   :scale: 100
   :align: right

.. automodule:: witkets.imagebutton
   :members:

ImageMap
========

.. autoclass:: witkets.ImageMap
    :members:

LED
===

.. figure:: img/led_screenshot.png
   :scale: 100
   :align: right

.. autoclass:: witkets.LED
   :members:

LEDBar
======

.. figure:: img/ledbar_screenshot.png
   :scale: 100
   :align: right

.. autoclass:: witkets.LEDBar
   :members:

LinkButton
==========

.. autoclass:: witkets.LinkButton
   :members:

LogicSwitch
===========

.. figure:: img/logicswitch_screenshot.png
   :scale: 100
   :align: right

.. autoclass:: witkets.LogicSwitch
    :members:

NumericLabel
============

.. figure:: img/numericlabel_screenshot.png
   :scale: 100
   :align: right

.. autoclass:: witkets.NumericLabel
    :members:
    
Plot (experimental)
===================

.. figure:: img/plot_screenshot.png
   :scale: 100
   :align: right

.. autoclass:: witkets.plot.Plot
    :members:

PyText
======

.. automodule:: witkets.pytext
    :members:

PyScrolledText
==============

.. automodule:: witkets.pyscrolledtext
    :members:
    
Ribbon
======

.. figure:: img/ribbon_screenshot.png
   :scale: 100
   :align: right

.. autoclass:: witkets.Ribbon
   :members:

Usage example
^^^^^^^^^^^^^

.. code-block:: python
   :linenos:

    import tkinter as tk
    import witkets as wtk
    root = tk.Tk()
    ribbon = wtk.Ribbon(root)
    tab1 = ribbon.add_tab('Main')
    group1 = tab1.create_h_group("Document")
    btn_new = group1.create_toolbar_item('data/document-new.png', 'New')
    btn_open = group1.create_toolbar_item('data/document-open.png', 'Open')
    btn_save = group1.create_toolbar_item('data/document-save.png', 'Save')
    group2, btn = tab1.create_h_group("Edit", corner=True)
    btn_cut = group2.create_toolbar_item('data/edit-cut.png', 'Cut')
    btn_copy = group2.create_toolbar_item('data/edit-copy.png', 'Copy')
    btn_paste = group2.create_toolbar_item('data/edit-paste.png', 'Paste')
    btn_new['command'] = lambda: print('Hello, world!')
    vgroup1 = group2.create_v_group([
        ('test/edit-undo.png', 'Undo'),
        ('test/edit-redo.png', 'Redo')
    ])
    tab2 = ribbon.add_tab('Insert')
    tab2.create_toolbar_item('test/run.png', 'Run')
    tab2.create_separator()
    ribbon.pack(fill='x')
    
    style = wtk.Style()
    wtk.Style.set_default_fonts()
    style.apply_default()

    root.mainloop()

Styles
^^^^^^

The following layouts are used by this widget:

 - Ribbon.TFrame
 - Ribbon.TNotebook
 - Ribbon.TNotebook.Tab
 - Ribbon.TButton
 - Ribbon.TLabel
 - RibbonBottom.TLabel
 - RibbonBottom.TFrame
 - RibbonBottom.TButton
 - RibbonGroup.TFrame
 - RibbonGroupInner.TFrame

Scope (experimental)
====================

.. figure:: img/scope_screenshot.png
   :scale: 100
   :align: right

.. autoclass:: witkets.plot.Scope
    :members:

Spin
====

.. figure:: img/spin_screenshot.png
   :scale: 100
   :align: right

.. autoclass:: witkets.Spin
    :members:

Spinner
=======

.. figure:: img/spinner_screenshot.png
   :scale: 100
   :align: right

.. autoclass:: witkets.Spinner
    :members:

Tank
====

.. figure:: img/tank_screenshot.png
   :scale: 100
   :align: right

.. autoclass:: witkets.Tank
    :members:

Thermometer
===========

.. figure:: img/thermometer_screenshot.png
    :scale: 100
    :align: right
    
.. autoclass:: witkets.Thermometer
    :members:


ThemedLabelFrame
================

.. autoclass:: witkets.ThemedLabelFrame
    :members:

TimeEntry
=========

.. figure:: img/timeentry_screenshot.png
    :scale: 100
    :align: right

.. autoclass:: witkets.TimeEntry
    :members:

ToggleButton
============

.. figure:: img/togglebutton_screenshot.png
   :scale: 100
   :align: right

.. autoclass:: witkets.ToggleButton
    :members:

Toolbar
=======

.. figure:: img/toolbar_screenshot.png
   :scale: 100
   :align: right

.. autoclass:: witkets.Toolbar
    :members:
    
