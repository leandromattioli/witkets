************
Introduction
************

Witkets is a collection of extensions for **tkinter** and **ttk**, featuring:

* a XML-based GUI builder,
* INI-based stylesheets,
* an application helper class (`witkets.Application`)
* many new widgets, ranging from LEDs and LogicSwitches to Tanks, Scopes, 
  Ribbon  toolbars and a text view with Python syntax highlighting.

For effective usage of this library, prior knowledge of **tkinter** basics is required.

Acquiring and Installing **witkets**
====================================

Requirements
------------

* Python 3.6+
* tkinter module
* pygments (optional; used in text widgets syntax highlighting)

Install by PyPI
---------------

The easiest way to install **witkets** is by using **pip** package manager.

Just run, as administrator:

:code:`# pip3 install witkets`

On Windows platforms that might be:

:code:`# pip install witkets`

In some Linux distributions the package is called **python3-pip**. On Windows
platforms the **pip3** binary is usually named **pip**.

Installing from the Latest Source Code
--------------------------------------

This package is hosted at BitBucket and its latest version can be retrieved with:

:code:`git clone https://bitbucket.org/leandromattioli/witkets.git`

It uses the standard Python installation script. You can install it with:

:code:`# python3 setup.py install`

Overview of **witkets**
=======================



Demo 1
======

Consider the following GUI application:

.. figure:: img/witkets_themed.png
   :scale: 100

With this library, you can use XML markup to describe the elements and the layout
of the interface and then use a builder to construct and place the widgets.

The source-code for this demo is given below:

**demo1.xml**

.. literalinclude :: workspace/demo1/demo1.xml
   :language: xml


**demo1.py**

.. literalinclude :: workspace/demo1/demo1.py
   :language: python

	