*****************************************************************
Dynamic Building Interfaces (:code:`witkets.tkbuilder.TkBuilder`)
*****************************************************************

.. TODO: Include pictures of GUIs designed with TkBuilder (with new widgets!)

.. role:: xml(code)
   :language: xml

Purpose
=======

This class is responsible for constructing Tk user interfaces based on XML
descriptions. It is somewhat similar to GtkBuilder, but without Glade (yet).

Basic Usage
===========

Consider the following XML string:

**demo2.xml**

.. literalinclude :: workspace/demo2/demo2.xml
   :language: xml

It describes children of a given Tk container (root element). For instance, when
building the interface, :code:`TkBuilder` will insert a label, which can be
accessed later by the identifier *lbl1*. This label will have the text property
set to *Cap. Kirk* and its background property set to *red*. In fact, for a 
given object :code:`obj`, most of the XML attributes (the *wid* being an
exception) are treated just like this:


.. code-block:: python

    obj[attrName] = attrValue

The special (and required) *wid* attribute is used for retrieving objects from the builder
once the GUI has been constructed. The special *geometry* tag is used to define
the GUI layout based on the Tk geometry managers: pack, grid and place. The
*geometry* tag must appear after the definition of all its referenced widgets,
just like the example above. In other words, it probably should be the last
child of a given container.

The Python snippet below should just build the interface described by 
**demo1.xml** into the main Tk frame:

**demo2.py**

.. literalinclude :: workspace/demo2/demo2.py
   :language: python


The builded GUI is shown below:

.. figure:: img/tkbuilder_example.png
   :scale: 100

Accessing the Widgets
=====================

After building the interface, you can access each widget with the nodes attribute:

.. code:: python

    label = builder['nodes']['lbl1']
    print(label['text']) # Cap. Kirk

Recognized Tags and Attributes
==============================

Although the :func:`~witkets.TkBuilder.add_tag` method can be used to
define a new tag and its corresponding class, TkBuilder comes with the
following builtin tags:

Widgets
-------
 
 * :xml:`<accellabel>`
 * :xml:`<button>`
 * :xml:`<canvas>`
 * :xml:`<cardlayout>`
 * :xml:`<checkbutton>`
 * :xml:`<colorbutton>`
 * :xml:`<combobox>`
 * :xml:`<consoleview>`
 * :xml:`<entry>`
 * :xml:`<expander>`
 * :xml:`<filechooserentry>`
 * :xml:`<frame>`
 * :xml:`<gauge>`
 * :xml:`<imagebutton>`
 * :xml:`<imagemap>`
 * :xml:`<label>`
 * :xml:`<labelframe>`
 * :xml:`<led>`
 * :xml:`<ledbar>`
 * :xml:`<linkbutton>`
 * :xml:`<listbox>`
 * :xml:`<logicswitch>`
 * :xml:`<menu>`
 * :xml:`<menubutton>`
 * :xml:`<message>`
 * :xml:`<notebook>`
 * :xml:`<numericlabel>`
 * :xml:`<panedwindow>`
 * :xml:`<plot>`
 * :xml:`<progressbar>`
 * :xml:`<pyscrolledtext>`
 * :xml:`<pytext>`
 * :xml:`<radiobutton>`
 * :xml:`<ribbon>`
 * :xml:`<scale>`
 * :xml:`<scope>`
 * :xml:`<scrollbar>`
 * :xml:`<scrolledtext>`
 * :xml:`<separator>`
 * :xml:`<sizegrip>`
 * :xml:`<spin>`
 * :xml:`<spinbox>`
 * :xml:`<spinner>`
 * :xml:`<tank>`
 * :xml:`<text>`
 * :xml:`<themedlabelframe>`
 * :xml:`<thermometer>`
 * :xml:`<timeentry>`
 * :xml:`<tkbutton>` (for `tk.Button` instead of `ttk.Button`)
 * :xml:`<tkpanedwindow>` (for `tk.PanedWindow` instead of `ttk.PanedWindow`)
 * :xml:`<togglebutton>`
 * :xml:`<toolbar>`
 * :xml:`<treeview>`

This list comprehends almost all basic Tk widgets, ttk widgets and the new 
widgets provided by the :code:`witkets` package.

For details on the widgets API, please refer to the 
:ref:`New Widgets section <new-widgets>`.

Containers, such as :class:`Frame` and :class:`CardLayout` share the 
same structure of the *root* elements: children (widgets) and geometry
definitions.

.. TODO add all widgets
 
Control
-------
 * :xml:`<root>`
 * :xml:`<geometry>`
    - :xml:`<pack>`
    - :xml:`<grid>`
    - :xml:`<place>`
    - :xml:`<tab>` (for `ttk.Notebook`)
    - :xml:`<pane>` (for `ttk.PanedWindow`)
    - :xml:`<card>` (for card layout)
 * :xml:`<style>` (inline or from external file)
 * :xml:`<grid-configure>` (for additional grid config)
 * :xml:`<variable>` and :xml:`<textvariable>` (inside widget tags)

.. TODO add samples for <variable> and <grid-configure>

.. _tkbuilder-style-tag:

Theme Specification
===================

A *style* tag can appear as a child of the *root* tag, requiring the theme's
INI specification to be either inside the tag or referenced to a file by the
*fromfile* attribute. In this second case, the path is relative to working
directory of Python. Also, if the attribute *defaultfonts* is defined and not
'0', all widget fonts will be set to 'Helvetica 11'. Finally, if the attribute
*applydefault* is given with some value other than '0' the default theme is
applied.

For further details, please refer to the section 
:ref:`Themes and Styling <theme-styling>`.

Example
-------

.. code-block:: xml

    <root>
        <style defaultfonts="1">
            [SpockLabel.TLabel]
                foreground=#088
                background=#000
        </style>
        <!--as the first XML example...-->
            <label wid="lbl3" text="Spock" style="SpockLabel.TLabel" />
        <!--as the first XML example...-->
    </root>

.. code-block:: xml

    <root>
    <style fromfile="styles.ini" />
    <!--your layout here-->
    </root>


Visual Designers
================

A visual designer named **tksimplegui** will eventually be released with 
limited supported for *witkets* XML.

API Reference
=============

.. automodule:: witkets.tkbuilder
   :members:

   .. autoclass:: witkets.TkBuilder
      :special-members: __init__
      :members:
