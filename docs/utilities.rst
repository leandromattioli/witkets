*********
Utilities
*********

GUI Application Helper
----------------------

.. automodule:: witkets.application
    :members:

Coordinate Normalization
------------------------

.. automodule:: witkets.coordsys
    :members: