.. _theme-styling:

.. role:: xml(code)
   :language: xml

******************************************
Themes and Styling (:code:`witkets.Style`)
******************************************

This class extends :class:`ttk.Style` to provide an alternate fashion of
defining styles. They are read from INI-like files or strings.

Usage options
=============

There are four approaches for the style definitions:

#) put the style definitions inside TkBuilder's :xml:`<style>` tag
   
   .. code-block:: xml

       <root borderwidth="15">
           <style>
               [Error.TLabel]
                   foreground=red
                   font=Helvetica 12 bold
           </style>
           <label wid="label1" text="This is an error!" style="Error.TLabel">
           <geometry>
               <pack for="label1">
           </geometry>
       </root>

   .. highlight:: python

#) use an external INI file and load it with Python
   
   .. code-block:: pycon

        >>> import witkets as wtk
        >>> # ...
        >>> style = wtk.Style()
        >>> style.apply_from_file('styles.ini') 

#) use an external INI file and reference it inside the :xml:`<style>` tag

   .. code-block:: xml

        <root borderwidth="15">
            <!-- Path relative to Python's working directory -->
            <style fromfile='styles.ini' />
            <label wid="label1" text="This is an error!" style="Error.TLabel">
            <geometry>
                <pack for="label1">
            </geometry>
        </root>

   .. highlight:: python

#) load a string directly with Python
   
   .. code-block:: pycon

       >>> import witkets as wtk
       >>> # ...
       >>> MY_STYLE = '''
       ... [Red.TLabel]
       ... foreground=red
       ... ''' 
       >>> style = wtk.Style()
       >>> style.apply_from_string(MY_STYLE)


Format
======

The format is quite simple: INI sections names represents ttk style names. 
Style values are passed as INI key--values. The example below shows a 
basic stylesheet:

**styles3a.ini**

.. literalinclude :: workspace/demo3/styles3b.ini
   :language: ini

In this simple case, most buttons will have a blue background (color #008)
except the ones having the style prefix BigButton (color #DDC). On MS 
Windows platforms, some native widgets are used and impose some limitations.
Some of the customizations, notably buttons background, are ignored. For workarounds,
follow the example on the :ref:`MS Windows section <ms-windows-theme-limitation>`.

Composed styles are used to deal with particular widget states and are 
described with the '.' character acting as a separator. 
This is shown in the in the fragment below.

**styles3b.ini (modified TButton definition)**

.. literalinclude :: workspace/demo3/styles3b.ini
   :language: ini

Please refer to :code:`tkinter.ttk` documentation or
`this tutorial <http://www.tkdocs.com/tutorial/styles.html>`
for a detailed explanation on widget style classes and attributes.

Example
=======

The snippet below should test the stylesheet provided in the last section:

**demo3.py**

.. literalinclude :: workspace/demo3/demo3.py
   :language: xml

The static method :func:`Style.set_default_fonts()` normalizes Tk fonts to
Helvetica 11. The rendered window is shown in the following figure.

.. Option :figclass: align-center affects only the image
.. figure:: img/theme_screenshot.png
   :scale: 100

Blue buttons -- :class:`witkets.Style` in action


The Default Theme
=================

A default stylesheet is provided and can be
used by calling the method :func:`~witkets.Style.apply_default`.

Also, its contents can be retrieved with :code:`importlib.resources`,
accessing the data file with
:code:`files('witkets').joinpath('data/default_theme.ini').read_bytes()`. However,
to be able to override some rules, it is sufficient to read a stylesheet
with the desired modifications, normally after applying the default theme.

Once the *witkets* package is installed, one can see the default theme style 
definitions by running the demo application:

.. code:: sh

    python3 -m witkets.scripts.demo


Labels styles screenshot
------------------------

.. figure:: img/default-theme-labels.png
   :scale: 100

Entries styles screenshot
-------------------------

.. figure:: img/default-theme-entries.png
   :scale: 100

Buttons styles screenshot
-------------------------

.. figure:: img/default-theme-buttons.png
   :scale: 100

Frames styles screenshot
------------------------

.. figure:: img/default-theme-frames.png
   :scale: 100

New styles
----------

The following styles are provided:

* Labels and Messages
    - Header.TLabel
    - Error.TLabel
    - Success.TLabel
    - Header.TMessage
    - Error.TMessage
    - Success.TMessage
* Entries
    - Invalid.TEntry
    - Incomplete.TEntry
* Buttons
    - Primary.TButton
    - Cancel.TButton
* Frames
    - Bordered.TFrame
* Witkets
    - MenuEntry.TLabel (AccelLabel widget)
    - Accelerator.TLabel (AccelLabel widget)
    - Expander.TButton
    - Ribbon.TButton
    - Ribbon.TLabel
    - Ribbon.TNotebook
    - Ribbon.TNotebook.Tab
    - Ribbon.TFrame
    - RibbonBottom.TLabel
    - RibbonBottom.TFrame
    - RibbonBottom.TButton
    - RibbonGroupInner.TFrame
    - RibbonGroup.TFrame
    - ThemedLabelFrame.TFrame
    - ThemedLabelFrame.TLabel
    - Toolbar.TFrame
    - Toolbar.TButton

Some new widgets provided by this package, notably :class:`ThemedLabelFrame`,
:class:`Expander` and :class:`Toolbar`, depends on styles defined by the 
default theme.

.. _ms-windows-theme-limitation:

MS Windows-specific considerations
==================================

Be aware of a **ttk** limitation on MS Windows platform preventing changes on
the background color of :class:`ttk.Button` instances when native widgets
are used. To properly change the button background on MS Windows, you might
want to call the :func:`theme_use` method before applying your custom styles.

.. code:: python

    >>> import tkinter as tk
    >>> import tkinter.ttk as ttk
    >>> import witkets as wtk
    >>> root = tk.Tk()
    >>> style = wtk.Style()
    >>> style.theme_use('clam') # choose one of the available themes
    >>> style.apply_from_file('styles3b.ini')
    >>> button = ttk.Button()
    >>> button.pack()
    >>> root.mainloop()

API Reference
=============

.. autoclass:: witkets.style.Style
  :special-members: __init__
  :members:
