import tkinter as tk
import tkinter.ttk as ttk
import witkets as wtk

overview_gui = '''
<root borderwidth='5'>
  <style>
    [White.TLabelframe]
    background=#FFF
    borderwidth=1
    relief=raised
    [White.TLabelframe.Label]
    background=#FFF
  </style>

  <!--First Frame-->
  <labelframe wid="frm1" text="Some Standard Tk Widgets">
    <button wid="btn1" text="Button" />
    <checkbutton wid="check1" text="Checkbutton!" />
    <entry wid="entry1" text="Entry" />
    <label wid="lbl1" text="Label" />
    <radiobutton wid="radio1" text="RadioButton" />
    <spinbox wid="spin1" from="0" to="255" width="3" />
    <scale wid="scale1" from="0" to="255" />
    <!--<menubutton wid="menubutton1" />-->
    <!--<panedwindow wid="panedwin1"> -->
    <!--<optionmenu wid="optionmenu1" /> not supported! -->
    <!--scrollbar not supported-->
	<geometry>
	    <pack for="btn1" side="left" padx="10"/>
	    <pack for="check1" side="left" padx="10" />
	    <pack for="entry1" side="left" padx="10" />
	    <pack for="lbl1" side="left" padx="10" />
	    <pack for="radio1" side="left" padx="10" />
	    <pack for="spin1" side="left" padx="10" />
	    <pack for="scale1" side="left" padx="10" />
	</geometry>
  </labelframe>
  
  <!--Second frame-->
  <labelframe wid="frm2" text="Other Standard Tk Widgets and Ttk">
    <text wid="text1" height="5"  width="30" />
    <message wid="msg1" text="Message:&#10;multiline text!" />
    <!--<listbox wid="list1" height="5" />-->
	<scrolledtext wid="scrolledtext1" height="5" width="40" />
    <geometry>
	    <pack for="msg1" side="left" padx="10" />
	    <pack for="text1" side="left" padx="10" />
	    <!--<pack for="list1" side="left" padx="10" />-->
	    <pack for="scrolledtext1" side="left" padx="10" />
    </geometry>
  </labelframe>
  
  
  <labelframe wid="frm3" text="Some Witkets" style='White.TLabelframe'>
    <led wid="led1" />
    <logicswitch wid="switch1" />
    <numericlabel wid="numlbl1" />
    <tank wid="tank1" />
    <plot wid="plot1" width="200" height="200" />
    <scope wid="scope1" width="200" height="200" />
    <geometry>
        <pack for="led1" side="left" padx="10"/>
        <pack for="switch1" side="left" padx="10"/>
        <pack for="numlbl1" side="left" padx="10"/>
        <pack for="tank1" side="left" padx="10"/>
        <pack for="plot1" side="left" padx="10"/>
        <pack for="scope1" side="left" padx="10"/>
    </geometry>
  </labelframe>
  
  
  <labelframe wid="frm4">
    <label wid="lbl2" 
           text="Widgets requiring reasonable further coding to be useful" />
    <label wid="lbl3" 
           text="Canvas, ComboBox, PanedWindow, MenuButton, ListBox" />
    <geometry>
        <pack for="lbl2" />
        <pack for="lbl3" />
    </geometry>
  </labelframe>
  
  
  <geometry>
    <pack for="frm1" ipadx="10" ipady="10" />
    <pack for="frm2" ipadx="10" ipady="10" />
    <pack for="frm3" ipadx="10" ipady="10" pady='10' />
    <pack for="frm4" ipadx="10" ipady="10" />
  </geometry>
  
</root>'''

root = tk.Tk()
s = wtk.Style()
s.theme_use('clam')
wtk.Style.set_default_fonts()
s.apply_default()
builder = wtk.TkBuilder(root)
builder.build_from_string(overview_gui)
root.mainloop()
