import os
import glob
import sys
import argparse

parser = argparse.ArgumentParser('publish.py')
parser.add_argument('--dist', action='store_true', help='Make dist tarballs')
parser.add_argument('--doc', action='store_true', help='Upload documentation')
parser.add_argument('--publish', action='store_true', help='Publish package')

args = parser.parse_args()
if not args.dist and not args.doc and not args.publish:
    print('Invalid usage!', file=sys.stderr)

if args.dist:
    # Making dist tarballs
    os.system('python setup.py sdist bdist bdist_egg')

if args.publish:
    # Uploading source distribution
    filenames = glob.glob('./dist/witkets-[0-9].[0-9].tar.gz')
    filenames.sort(reverse=True)
    last_release = filenames[0]
    os.system('twine upload ' + last_release)

if args.doc:
    # Generating documentation
    os.system('cd docs && make html')
    target = os.getenv('WITKETS_DOC_TARGET')
    if not target:
        target = input('Enter path to upload: ')
    port = os.getenv('WITKETS_DOC_SCP_PORT')
    if not port:
        port = input('Enter SCP Port: ')
    os.system('scp -P %s -r docs/_build/html %s' % (port, target))
