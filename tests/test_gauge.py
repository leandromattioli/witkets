import unittest
import tkinter as tk
import witkets as wtk
from basetest import BaseTest

GUI_GAUGE = '''
<root>
    <gauge wid='gauge1' minvalue='0' maxvalue='100' tickcount='8'
            width='96' height='96' startang='30' endang='330'
            knobcolor='red' />
    <geometry>
        <pack for='gauge1' />
    </geometry>
</root>
'''

class TestGauge(BaseTest):
    
    def test_imperative(self):
        # Test constructor 1
        gauge = wtk.Gauge(self.root, 
            number = 30,
            maxvalue = 50,
            minvalue = -50,
            tickcount = 10,
            startang = 45,
            endang = 270,
            knobrelradius = 0.6,
            knobcolor = 'red',
            scalecolor = 'blue',
            tickcolor = 'white',
            cursorcolor = 'yellow',
            cursorwidth = 10
        )
        gauge.pack()
        self.assertEqual(gauge['number'], 30)
        self.assertEqual(gauge['maxvalue'], 50)
        self.assertEqual(gauge['minvalue'], -50)
        self.assertEqual(gauge['tickcount'], 10)
        self.assertEqual(gauge['startang'], 45)
        self.assertEqual(gauge['endang'], 270)
        self.assertEqual(gauge['knobrelradius'], 0.6)
        self.assertEqual(gauge['knobcolor'], 'red')
        self.assertEqual(gauge['scalecolor'], 'blue')
        self.assertEqual(gauge['tickcolor'], 'white')
        self.assertEqual(gauge['cursorcolor'], 'yellow')
        self.assertEqual(gauge['cursorwidth'], 10)
        # Test config()
        gauge.config(
            number = 31,
            maxvalue = 51,
            minvalue = -51,
            tickcount = 11,
            startang = 50,
            endang = 280,
            knobrelradius = 0.4,
            knobcolor = 'green',
            scalecolor = 'yellow',
            tickcolor = 'blue',
            cursorcolor = 'white',
            cursorwidth = 12
        )
        self.assertEqual(gauge['number'], 31)
        self.assertEqual(gauge['maxvalue'], 51)
        self.assertEqual(gauge['minvalue'], -51)
        self.assertEqual(gauge['tickcount'], 11)
        self.assertEqual(gauge['startang'], 50)
        self.assertEqual(gauge['endang'], 280)
        self.assertEqual(gauge['knobrelradius'], 0.4)
        self.assertEqual(gauge['knobcolor'], 'green')
        self.assertEqual(gauge['scalecolor'], 'yellow')
        self.assertEqual(gauge['tickcolor'], 'blue')
        self.assertEqual(gauge['cursorcolor'], 'white')
        self.assertEqual(gauge['cursorwidth'], 12)
        # Test __setitem__
        gauge['number'] = 32
        gauge['maxvalue'] = 52
        gauge['minvalue'] = -52
        gauge['tickcount'] = 12
        gauge['startang'] = 60
        gauge['endang'] = 290
        gauge['knobrelradius'] = 0.3
        gauge['knobcolor'] = '#CCCCCC'
        gauge['scalecolor'] = '#800000'
        gauge['tickcolor'] = 'green'
        gauge['cursorcolor'] = 'purple'
        gauge['cursorwidth'] = 4
        self.assertEqual(gauge['number'], 32)
        self.assertEqual(gauge['maxvalue'], 52)
        self.assertEqual(gauge['minvalue'], -52)
        self.assertEqual(gauge['tickcount'], 12)
        self.assertEqual(gauge['startang'], 60)
        self.assertEqual(gauge['endang'], 290)
        self.assertEqual(gauge['knobrelradius'], 0.3)
        self.assertEqual(gauge['knobcolor'], '#CCCCCC')
        self.assertEqual(gauge['scalecolor'], '#800000')
        self.assertEqual(gauge['tickcolor'], 'green')
        self.assertEqual(gauge['cursorcolor'], 'purple')
        self.assertEqual(gauge['cursorwidth'], 4)
        # Check tkinter update
        self.root.update()

    def test_declarative(self):
        builder = wtk.TkBuilder(self.root)
        builder.build_from_string(GUI_GAUGE)
        self.root.update()


if __name__ == '__main__':
    unittest.main()