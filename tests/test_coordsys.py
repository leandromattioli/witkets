import unittest
import math
import tkinter as tk
import witkets as wtk


class TestCoordSys(unittest.TestCase):
    def test_coord_axis(self):
        # Constructor
        err = 'Constructor failed!'
        coord_conv = wtk.CoordConverter(-10, 10)
        self.assertEqual(coord_conv.minval, -10, err)
        self.assertEqual(coord_conv.maxval, 10, err)
        self.assertFalse(coord_conv.inverted, err)
        # Conversions
        err = 'NDC conversion failed!'
        ndc = coord_conv.to_ndc(0)
        self.assertAlmostEqual(ndc, 0.5, 1, err)
        ndc = coord_conv.to_ndc(-10)
        self.assertAlmostEqual(ndc, 0, 1, err)
        x = coord_conv.from_ndc(0.5)
        err = 'WCS conversion failed!'
        self.assertAlmostEqual(x, 0, err)
        x = coord_conv.from_ndc(0)
        self.assertAlmostEqual(x, -10, err)
        err = 'in range failed!'
        self.assertTrue(coord_conv.in_range(-10), err)
        self.assertTrue(coord_conv.in_range(5), err)
        self.assertFalse(coord_conv.in_range(11), err)

    def test_inverted_coord_axis(self):
        coord_conv = wtk.CoordConverter(-5, 5, inverted=True)
        ndc = coord_conv.to_ndc(5)
        self.assertAlmostEqual(ndc, 0, 1, 'NDC conversion failed!')
        x = coord_conv.from_ndc(1)
        self.assertAlmostEqual(x, -5, 1, 'WCS conversion failed!')

    def test_coord_sys(self):
        coord_sys = wtk.CoordSys2D(-10, 10, -20, 18, False, True)
        err = '2D NDC conversion failed!'
        x, y = coord_sys.from_ndc(0.5, 1)
        self.assertAlmostEqual(x, 0, err)
        self.assertAlmostEqual(y, -20, err)
        err = '2D WCS conversion failed!'
        xn, yn = coord_sys.to_ndc(5, 18)
        self.assertAlmostEqual(xn, 0.75, err)
        self.assertAlmostEqual(yn, 0, err)
        err = '2D in range failed!'
        self.assertFalse(coord_sys.in_range(-12, 20), err)
        self.assertFalse(coord_sys.in_range(-12, 18), err)
        self.assertFalse(coord_sys.in_range(-10, 20), err)
        self.assertTrue(coord_sys.in_range(-10, 18), err)

if __name__ == '__main__':
    unittest.main()

