import unittest
import tkinter as tk
import witkets as wtk
from basetest import BaseTest

GUI_CARD_LAYOUT = '''
<root>
    <cardlayout wid='card1'>
        <label wid='label1' text='Some content...' />
        <label wid='label2' text='Other content...' />
        <frame wid='frame1' />
        <geometry>
            <card for='label1' name='first' />
            <card for='label2' name='second' />
            <card for='frame1' />
        </geometry>
    </cardlayout>
    <button wid='button1' text='Change card...' />
    <geometry>
        <pack for='card1' />
        <pack for='button1' />
    </geometry>
</root>
'''

class TestCardLayout(BaseTest):

    def visible(self, widget):
        self.root.update()
        return widget.winfo_ismapped()

    def assertMutualExclusion(self, widgets: list):
        visibles = [w for w in widgets if self.visible(w)]
        self.assertEqual(len(visibles), 1, 'Multiple cards visible!')

    def assertCard(self, widget, msg=None):
        self.assertEqual(self.visible(widget), True, msg)
        self.assertMutualExclusion(self.widgets)
    
    def test_imperative(self):
        cards = wtk.CardLayout(self.root)
        cards.pack()
        label1 = tk.Label(cards, text='Label 1')
        label2 = tk.Label(cards, text='Label 2')
        label3 = tk.Label(cards, text='Label 3')
        cards.add(label1, name='First Card')
        cards.add(label2)
        cards.add(label3, name='Last Card')
        # Test mutually exclusive
        self.widgets = (label1, label2, label3)
        self.assertMutualExclusion(self.widgets)
        # Test last card
        cards.last()
        self.assertCard(label3, msg='last() failed')
        # Test first card
        cards.first()
        self.assertCard(label1, msg='first() failed')
        # Test previous
        cards.previous()
        self.assertCard(label3, msg='circular decrement failed!')
        cards.previous()
        self.assertCard(label2, msg='decrement failed!')
        # Test next
        cards.next()
        self.assertCard(label3, msg='increment failed!')
        cards.next()
        self.assertCard(label1, msg='circular increment failed!')
        # Check tkinter update
        self.root.update()

    def test_declarative(self):
        builder = wtk.TkBuilder(self.root)
        builder.build_from_string(GUI_CARD_LAYOUT)
        self.root.update()


if __name__ == '__main__':
    unittest.main()