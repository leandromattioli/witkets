import unittest
import tkinter as tk
import witkets as wtk
from basetest import BaseTest

GUI_FILECHOOSERENTRY = '''
<root>
    <filechooserentry wid='filechooser1' buttontext="Choisir"
        dialogtitle="Nom du nouveau fichier..."
        action="save">
        <filetypes>
            <filetype name="Des images" pattern="*.png" />
        </filetypes>
    </filechooserentry>
    <geometry>
        <pack for='filechooser1' />
    </geometry>
</root>
'''

class TestFileChooserEntry(BaseTest):
    
    def test_imperative(self):
        # Test constructor 1
        filechooser = wtk.FileChooserEntry(self.root, buttontext='Test1')
        filechooser.pack()
        self.assertEqual(filechooser.button['text'], 'Test1')
        fake_path = '/home/user/test.txt'
        filechooser['textvariable'].set(fake_path)
        self.assertEqual(filechooser.entry.get(), fake_path)
        # Test config()
        filechooser.config(buttontext='Other text')
        self.assertEqual(filechooser.button['text'], 'Other text')
        # Test __setitem__
        filechooser['buttontext'] = 'Final text'
        self.assertEqual(filechooser.button['text'], 'Final text')
        # Check tkinter update
        self.root.update()

    def test_declarative(self):
        builder = wtk.TkBuilder(self.root)
        builder.build_from_string(GUI_FILECHOOSERENTRY)
        self.root.update()


if __name__ == '__main__':
    unittest.main()