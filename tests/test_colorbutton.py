import unittest
import tkinter as tk
import witkets as wtk
from basetest import BaseTest

GUI_COLOR_BUTTON = '''
<root>
    <label wid='label1' text='Colors:' />
    <colorbutton wid='colorbutton1' color='#800000' />
    <colorbutton wid='colorbutton2'>
        <textvariable name='color2' type='text' value='#008000' />
    </colorbutton>
    <geometry>
        <pack for='label1' side='left' />
        <pack for='colorbutton1' side='left' />
        <pack for='colorbutton2' side='left' />
    </geometry>
</root>
'''

class TestColorButton(BaseTest):
    
    def test_imperative(self):
        # Test constructor 1
        colorbtn = wtk.ColorButton(self.root, color='#CCCC00')
        self.assertEqual(colorbtn['color'], '#CCCC00')
        # Test constructor 2
        var = tk.StringVar(self.root, 'green')
        colorbtn2 = wtk.ColorButton(self.root, color='red', textvariable=var)
        self.assertEqual(colorbtn2['color'], 'red')
        self.assertEqual(colorbtn2['textvariable'], var)
        # Test variable
        colorbtn['textvariable'].set('orange')
        self.assertEqual(colorbtn['color'], 'orange')
        # Test config()
        colorbtn.config(color='blue', textvariable=var)
        self.assertEqual(colorbtn['textvariable'], var)
        self.assertEqual(colorbtn['color'], 'blue')
        # Test __setitem__
        var2 = tk.StringVar(self.root, 'blue')
        colorbtn['color'] = 'black'
        colorbtn['textvariable'] = var2
        self.assertEqual(colorbtn['color'], 'blue') # from textvariable
        self.assertEqual(colorbtn['textvariable'], var2)
        # Check tkinter update
        self.root.update()

    def test_declarative(self):
        builder = wtk.TkBuilder(self.root)
        builder.build_from_string(GUI_COLOR_BUTTON)
        self.root.update()


if __name__ == '__main__':
    unittest.main()