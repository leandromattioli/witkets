import unittest
import tkinter as tk
import witkets as wtk
from basetest import BaseTest

GUI_ACCEL_LABEL = '''
<root>
    <accellabel wid='lbl-new' labeltext='New' acceltext='CTRL+N' />
    <accellabel wid='lbl-open' labeltext='Open...' acceltext='CTRL+O' />
    <geometry>
        <pack for='lbl-new' />
        <pack for='lbl-open' />
    </geometry>
</root>
'''

class TestAccelLabel(BaseTest):
    
    def test_imperative(self):
        # Test constructor
        accel = wtk.AccelLabel(self.root, labeltext='Copy', acceltext='CTRL+C')
        self.assertEqual(accel['labeltext'], 'Copy')
        self.assertEqual(accel['acceltext'], 'CTRL+C')
        # Test config()
        accel.config(labeltext='Paste', acceltext='CTRL+V')
        self.assertEqual(accel['labeltext'], 'Paste')
        self.assertEqual(accel['acceltext'], 'CTRL+V')
        # Test __setitem__
        accel['labeltext'] = 'Cut'
        accel['acceltext'] = 'CTRL+X'
        self.assertEqual(accel['labeltext'], 'Cut')
        self.assertEqual(accel['acceltext'], 'CTRL+X')
        # Test properties
        self.assertEqual(accel['labeltext'], accel.label['text'])
        self.assertEqual(accel['acceltext'], accel.accel['text'])
        # Check tkinter update
        self.root.update()

    def test_declarative(self):
        builder = wtk.TkBuilder(self.root)
        builder.build_from_string(GUI_ACCEL_LABEL)
        self.root.update()


if __name__ == '__main__':
    unittest.main()