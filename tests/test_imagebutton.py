import unittest

import tkinter as tk
import witkets as wtk

from basetest import BaseTest

GUI_IMAGE_BUTTON = '''
<root>
    <imagebutton wid='imgbutton1' imgfile='tests/data/cat1.png' text='Cat 1' compound='bottom' />
    <imagebutton wid='imgbutton2' imgfile='tests/data/cat2.png' text='Cat 2' compound='top' />
    <geometry>
        <pack for='imgbutton1' side='left' />
        <pack for='imgbutton2' side='left' />
    </geometry>
</root>
'''

class TestImageButton(BaseTest):
    
    def test_imperative(self):
        # Test constructor 1
        imagebtn = wtk.ImageButton(self.root, imgfile='tests/data/cat1.png', 
                                   compound=tk.LEFT)
        self.assertEqual(imagebtn['imgfile'], 'tests/data/cat1.png')
        # Test config()
        imagebtn.config(imgfile='tests/data/cat2.png', compound=tk.TOP)
        self.assertEqual(imagebtn['imgfile'], 'tests/data/cat2.png')
        # Test __setitem__
        imagebtn['imgfile'] = 'tests/data/cat1.png'
        self.assertEqual(imagebtn['imgfile'], 'tests/data/cat1.png')
        # Check tkinter update
        self.root.update()

    def test_declarative(self):
        builder = wtk.TkBuilder(self.root)
        builder.build_from_string(GUI_IMAGE_BUTTON)
        self.root.update()


if __name__ == '__main__':
    unittest.main()