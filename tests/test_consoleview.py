import unittest
import tkinter as tk
import witkets as wtk
from basetest import BaseTest

GUI_CONSOLEVIEW = '''
<root>
    <style>
        [Console.TEntry]
            foreground=blue
    </style>
    <consoleview wid='console1' />
    <geometry>
        <pack for='console1' />
    </geometry>
</root>
'''

class TestConsoleView(BaseTest):

    def send_command(self, command):
        self.consoleview._var_command.set(command)
        self.consoleview._on_return_key(None)
        self.root.update()

    def on_new_command(self, *event):
        self.last_command = self.consoleview.last_command
    
    def test_imperative(self):
        consoleview = wtk.ConsoleView(self.root)
        self.consoleview = consoleview
        # Test initial last_command
        self.assertEqual(consoleview.last_command, None)
        # Test <<NewCommand>> event
        consoleview.bind('<<NewCommand>>', self.on_new_command)
        self.send_command('test command')
        self.assertEqual(self.last_command, 'test command')
        # Test next command
        self.send_command('other command')
        self.assertEqual(consoleview.last_command, 'other command')
        # FIXME cannot test FOCUS, SENSIVITY and readline emulation
        # Check tkinter update
        self.root.update()

    def test_declarative(self):
        builder = wtk.TkBuilder(self.root)
        builder.build_from_string(GUI_CONSOLEVIEW)
        self.root.update()


if __name__ == '__main__':
    unittest.main()