import unittest
import tkinter as tk

class BaseTest(unittest.TestCase):
    
    def setUp(self):
        self.root = tk.Toplevel()

    def tearDown(self):
        self.root.destroy()
        self.root = tk.Toplevel()

if __name__ == '__main__':
    unittest.main()