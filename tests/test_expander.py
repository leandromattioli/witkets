import unittest
import tkinter as tk
import witkets as wtk
from basetest import BaseTest

GUI_EXPANDER = '''
<root>
    <expander wid='expander1' text='Test (XML)'>
        <label wid='lbl1' text='Contents' />
        <geometry>
            <pack for='lbl1' />
        </geometry>
    </expander>
    <geometry>
        <pack for='expander1' />
    </geometry>
</root>
'''

class TestExpander(BaseTest):
    
    def test_imperative(self):
        # Test constructor 1
        expander = wtk.Expander(self.root, text='Test')
        expander.pack()
        self.assertEqual(expander['text'], 'Test')
        self.assertTrue(expander['expanded'])
        # Test constructor 2
        expander2 = wtk.Expander(self.root, expanded=False)
        expander2.pack()
        self.assertFalse(expander2['expanded'])
        # Test config()
        expander.config(text='Other text', expanded=False)
        self.assertEqual(expander['text'], 'Other text')
        self.assertFalse(expander['expanded'])
        # Test __setitem__
        expander['text'] = 'Final text'
        expander['expanded'] = True
        self.assertEqual(expander['text'], 'Final text')
        self.assertTrue(expander['expanded'])
        # Check tkinter update
        self.root.update()

    def test_declarative(self):
        builder = wtk.TkBuilder(self.root)
        builder.build_from_string(GUI_EXPANDER)
        self.root.update()


if __name__ == '__main__':
    unittest.main()