#!/usr/bin/env python3

import sys
import re
import xml.etree.ElementTree as ElementTree
import tkinter as tk
import tkinter.ttk as ttk
from tkinter.scrolledtext import ScrolledText
from copy import copy
import witkets as wtk
from witkets.core import wtk_types
from witkets.core.attribspec import attribspec
from witkets.core import helpers
import witkets.plot as plt

#FIXME: Add built-in support for PhotoImages


_ERROR_VARIABLE_INVALID = '''
Either ref or name and type are required for variables!
'''.strip()


spinbox_cls = None
try:
    from tkinter.ttk import Spinbox
    spinbox_cls = Spinbox
except ImportError:  # Python 3.6: ttk.Spinbox not available!
    spinbox_cls = tk.Spinbox


# =========================================================================
# Module Symbols
# =========================================================================


_widget_classes = [
    # TK Base and TTK Overriden Widgets
    ttk.Button, tk.Canvas, ttk.Checkbutton, ttk.Entry, ttk.Frame, ttk.Label,
    ttk.LabelFrame, tk.Listbox, tk.Menu, tk.Message, ttk.Menubutton,
    ttk.PanedWindow, ttk.Radiobutton, ttk.Scale, ttk.Scrollbar,
    tk.scrolledtext.ScrolledText, spinbox_cls, tk.Text,
    # TTK Exclusive
    ttk.Combobox, ttk.Notebook, ttk.Progressbar, ttk.Separator, ttk.Sizegrip,
    ttk.Treeview,
    # Witkets
    wtk.AccelLabel, wtk.CardLayout, wtk.ColorButton, wtk.ConsoleView, 
    wtk.Expander, wtk.FileChooserEntry, wtk.Gauge, wtk.ImageButton, 
    wtk.ImageMap, wtk.LED, wtk.LEDBar, wtk.LinkButton, wtk.LogicSwitch, 
    wtk.NumericLabel, plt.Plot, plt.Scope, wtk.PyText, wtk.PyScrolledText, 
    wtk.Ribbon, wtk.Spinner, wtk.Spin, wtk.Tank, wtk.ThemedLabelFrame,
    wtk.Thermometer, wtk.TimeEntry, wtk.Toolbar, wtk.ToggleButton,
    # Others
    #tk.PhotoImage
]


_geometry_tags = ('pack', 'grid', 'place', 'card', 'tab', 'pane')


_containers = [
    'root', 'frame', 'labelframe', 'expander',
    'themedlabelframe', 'cardlayout', 'notebook',
    'panedwindow', 'toolbar'
]


# Properties that can be specified only during object initialization
_constructor_properties = {
    ttk.PanedWindow: ['orient'],
    wtk.Spin: ['orientation'],
    wtk.Toolbar: ['orientation']
}

# =========================================================================
# Builder
# =========================================================================

class TkBuilder:
    def __init__(self, master):
        """Initializer
        
            :param master:
                Tk root container where interface is going to be built
        """
        self._tree = None     # the XML tree
        self._root = None     # root container
        self._master = master # container
        self.nodes = {}       # widgets constructed via XML
        self.vars = {}        # TK variables definitions contained in XML tags
        self.tkstyle = None   # Style (for handling inline or external styles)
        self._tag2tk = {cls.__name__.lower(): cls for cls in _widget_classes}
        # Custom XML attributes handlers
        self._special_attribs = {
            (None, 'image'): self._handle_image_attribute # image attribute in all widgets
        }  
        self._special_children = {} # custom XML child tags handlers
        self._containers = _containers.copy() # can be modified by the user
        self.add_tag('tkbutton', tk.Button)
        self.add_tag('tkradiobutton', tk.Radiobutton)
        self.add_tag('tkpanedwindow', tk.PanedWindow, container=True)
        self.set_custom_child_tag('toolbar', 'toolbar-button', self._add_toolbar_button)

    # =========================================================================
    # Widget Tags
    # =========================================================================

    def _handle_widget(self, widget_node, parent):
        """Handles individual widgets tags."""
        try:
            wid = widget_node.attrib.pop('wid')
        except KeyError:
            print('Required key "wid" not found in %s' % widget_node.tag, 
                  file=sys.stderr)
            return
        # Canonical WID (example grandparent.parent.child)
        if wid.startswith('.') and 'wid' in parent.attrib:
            wid = parent.attrib['wid'] + wid
        # Creating widget        
        tk_class = self._tag2tk[widget_node.tag]
        if parent == self._root:
            parent_widget = self._master
        else:
            parent_id = parent.attrib['wid']
            parent_widget = self.nodes[parent_id]
        # Expander container
        if parent.tag == 'expander':
            parent_widget = parent_widget.frame
        # Attributes
        attributes = widget_node.attrib
        constructor_kwargs = {}
        # Constructor-only attributes
        if tk_class in _constructor_properties:
            needle = attributes.copy()
            for a, v in needle.items():
                if a in _constructor_properties[tk_class]:
                    constructor_kwargs[a] = v
                    attributes.pop(a, False)
        self.nodes[wid] = tk_class(parent_widget, **constructor_kwargs)
        # Mapping other attributes
        kwargs = self._get_attributes_dict(self.nodes[wid], widget_node.tag,
                                           attributes)
        self.nodes[wid].config(**kwargs)
        for child in widget_node:
            self._handle_widget_child_tag(wid, widget_node, child)

    def _handle_widget_child_tag(self, wid, widget_node, child_node):
        """Handles special widget config tags"""
        widget = self.nodes[wid]
        child_tag = child_node.tag
        if child_tag in ('variable', 'textvariable', 'numvariable'):
            self._handle_child_tkvar(wid, widget, child_node)
        elif child_tag=='filetypes' and isinstance(widget, wtk.FileChooserEntry):
            self._handle_filechooser_formats(wid, widget, child_node)
        elif (widget_node.tag, child_tag) in self._special_children:
            self._special_children[(widget_node.tag, child_tag)](
                widget, child_node
            )
        else:
            self._handle_child_html(wid, widget, child_node)

    # =========================================================================
    # Widget Child Tags
    # =========================================================================

    def _handle_child_tkvar(self, wid, widget, child_node):
        if 'ref' not in child_node.attrib and \
            'name' not in child_node.attrib and \
            'type' not in child_node.attrib:
            raise ValueError(_ERROR_VARIABLE_INVALID)
        # Reference to an existing variable
        if 'ref' in child_node.attrib:
            var = self.vars[child_node.attrib['ref']]
        # New variable
        else:
            name = child_node.attrib['name']
            type_ = child_node.attrib['type']
            defval = None
            if 'value' in child_node.attrib:
                defval = child_node.attrib['value']
            if type_ == 'int':
                var = tk.IntVar(value=defval)
            elif type_ == 'double':
                var = tk.DoubleVar(value=defval)
            elif type_ == 'text':
                var = tk.StringVar(value=defval)
            else:
                xml_fragment = ElementTree.tostring(child_node).decode('utf8')
                raise Exception('Invalid variable type! ' + xml_fragment)
            self.vars[name] = var
        widget[child_node.tag] = var

    def _handle_filechooser_formats(self, wid, widget, child_node):
        filetypes = []
        for child in child_node:
            if child.tag == 'filetype':
                if 'name' not in child.attrib or 'pattern' not in child.attrib:
                    raise Exception('Invalid file type (must contain "name" and "pattern" attributes)')
                filetypes.append(
                    (child.attrib['name'], child.attrib['pattern'])
                )
        widget['filetypes'] = filetypes

    def _handle_child_html(self, wid, widget, child_node):
        #HTML-like handling
        text = (child_node.text or '').strip() + ''.join(
            [ElementTree.tostring(e, 'unicode').strip() for e in child_node]
        )
        # tostring() adds an unwanted \n whenever <br> is found
        text = re.sub('<br( )*(/)?>\n', '<br/>', text)
        text = text.replace('\r', ' ').replace('\n', ' ')
        text = re.sub(' +', ' ', text)
        # now <br/> can be safely replaced by \n
        text = text.replace('<br/>', '\n')
        keyval = { child_node.tag : text }
        self.nodes[wid].config(**keyval)


    def _add_toolbar_button(self, widget, child_node):
        name = child_node.attrib.get('name', None)
        widget.add_button(name=name, imagepath=child_node.attrib['image'])

    # =========================================================================
    # Attributes
    # =========================================================================

    def _handle_image_attribute(self, widget, key, value):
        """Handles the special 'image' attribute, whose value can be specified in many ways.
        
        Currently, valid values for images are file paths or package resources, the 
        latter having the format `res://package/resource` 
        (see function `witkets.core.helpers.read_asset`)."""
        content = helpers.read_asset(value)
        widget.wtk_image = tk.PhotoImage(data=content) # assign to prevent garbage collector
        widget['image'] = widget.wtk_image


    def _get_attributes_dict(self, widget, tagname, attribs):
        """Handles attributes, except TkBuilder related"""
        # Removing editor-specific attributes
        attribs = {key : attribs[key] for key in attribs \
                                      if not key.startswith('wtk-editor-')}
        attribs = self._get_attribs_values(widget, tagname, attribs)
        return attribs

    def _get_attribs_values(self, widget, tagname, attribs):
        widcls = widget.__class__
        handlers = {}
        # For geometry tags like <pack> and <grid>
        if tagname in attribspec:
            handlers = attribspec[tagname]
        # For Tk and Ttk widgets
        elif widcls in attribspec:
            handlers = attribspec[widcls]
        # Retrieving attributes values
        typed_attribs = {}
        # this is complex, at least for now (maybe forever!)...
        for key in attribs: 
            # Specific rule takes precedence...
            if (tagname, key) in self._special_attribs:
                self._special_attribs[(tagname, key)](widget, key, attribs[key])
            # Over generic rule...
            elif (None, key) in self._special_attribs:
                self._special_attribs[(None, key)](widget, key, attribs[key])
            # Tk and Ttk Handlers
            elif key in handlers:
                typed_attribs[key] = TkBuilder._parse_or_construct(
                    handlers[key],
                    attribs[key]
                )
            # Witkets keys
            elif hasattr(widcls, 'widget_keys') and key in widcls.widget_keys:
                typed_attribs[key] = TkBuilder._parse_or_construct(
                    widcls.widget_keys[key], attribs[key]
                )
            # Enumerations
            elif key in dir(wtk_types):
                enum = getattr(wtk_types, key)
                try:
                    typed_attribs[key] = enum(attribs[key]).value
                except ValueError as ex:
                    params = (widget, tagname, key, ex)
                    print('[warning] [%s, %s, %s] %s' % params, file=sys.stderr)
                    continue
            # Generic keys
            else:
                typed_attribs[key] = attribs[key]
        return typed_attribs

    @staticmethod
    def _parse_or_construct(obj, value):
        # Duck typing (presuming a parse staticmethod is defined)...
        try:
            return obj.parse(value)
        # If not, it's just a simple type, like int
        except:
            return obj(value)


    # =========================================================================
    # Containers and Geometry
    # =========================================================================

    def _handle_container(self, container, parent):
        """Handles containers (<root>, <frame>, etc. and user-defined containers)"""
        wid = None
        attribs = copy(container.attrib)
        if container.tag != 'root':
            if container.tag not in self._tag2tk:
                print('Tag not supported: %s' % container.tag, file=sys.stderr)
                return
            if 'wid' not in attribs:
                print('Required key "wid" not found in %s' % container.tag,
                      file=sys.stderr)
                return
            wid = attribs.pop('wid')
            if wid.startswith('.') and 'wid' in parent.attrib:
                wid = parent.attrib['wid'] + wid
            container.attrib['wid'] = wid
            tk_class = self._tag2tk[container.tag]
            if parent != self._root:
                parent_id = parent.attrib['wid']
                parent_widget = self.nodes[parent_id]
            else:
                parent_widget = self._master
            # Popping constructor-only properties
            kwargs = {}
            if tk_class in _constructor_properties:
                for p in _constructor_properties[tk_class]:
                    if p in attribs:
                        kwargs[p] = attribs[p]
                        attribs.pop(p)
            self.nodes[wid] = tk_class(parent_widget, **kwargs)
            kwargs = self._get_attributes_dict(self.nodes[wid], container.tag, 
                attribs)
            self.nodes[wid].config(**kwargs)
            container_widget = self.nodes[wid]
        else:
            attribs = container.attrib
            kwargs = self._get_attributes_dict(self._master, 'root', attribs)
            self._master.config(**kwargs)
            container_widget = self._master
        # Container children
        for child in container:
            if child.tag in self._containers:
                self._handle_container(child, container)
            elif child.tag == 'geometry':
                self._handle_geometry(child, wid)
            elif child.tag == 'style':
                self._handle_stylesheet(child)
            elif child.tag == 'grid-configure':
                self._handle_grid_configure(container_widget, child)
            elif child.tag in self._tag2tk.keys():
                self._handle_widget(child, container)
            elif (container.tag, child.tag) in self._special_children:
                self._special_children[(container.tag, child.tag)](
                    container_widget, child
                )
            else:
                print(f'Invalid tag: {child.tag}!', file=sys.stderr)

    def _handle_geometry(self, geometry, container_wid):
        """Handle the special <geometry> tag"""
        for child in geometry:
            if child.tag not in _geometry_tags:
                print('Invalid geometry instruction %s' % child.tag, 
                      file=sys.stderr)
                continue
            attribs = copy(child.attrib)
            # Getting widget ID
            try:
                wid = attribs.pop('for')
            except KeyError:
                print('[geom] Required key "for" not found in %s' % child.tag,
                      file=sys.stderr)
                continue
            if wid.startswith('.') and container_wid:
                wid = container_wid + wid
            # Calling appropriate geometry method
            attribs = self._get_attribs_values(self.nodes[wid], child.tag, attribs)
            if child.tag == 'pack':
                self.nodes[wid].pack(**attribs)
            elif child.tag == 'grid':
                self.nodes[wid].grid(**attribs)
            elif child.tag == 'place':
                self.nodes[wid].place(**attribs)
            elif child.tag == 'card':
                name = None
                if 'name' in attribs:
                    name = attribs.pop('name')
                self.nodes[wid].master.add(self.nodes[wid], name=name, **attribs)
            elif child.tag in ('tab', 'pane'):
                self.nodes[wid].master.add(self.nodes[wid], **attribs)

    def _handle_grid_configure(self, parent_widget, child):
        """Handle grid configure commands.
            @FIXME: Need to be tested both in root and child frame 
        """
        for config_item in child:
            index = config_item.attrib.pop('index')
            if config_item.tag == 'row':
                parent_widget.grid_rowconfigure(int(index), **config_item.attrib)
            elif config_item.tag == 'column':
                parent_widget.grid_columnconfigure(int(index), **config_item.attrib)

    # =========================================================================
    # Styling
    # =========================================================================

    def _handle_stylesheet(self, style):
        """Handle the special <style> tag"""
        self.tkstyle = wtk.Style()
        if 'defaultfonts' in style.attrib and \
                style.attrib['defaultfonts'] != '0':
            wtk.Style.set_default_fonts()
        if 'applydefault' in style.attrib and \
                style.attrib['applydefault'] != '0':
            self.tkstyle.apply_default()
        if 'fromfile' in style.attrib:
            self.tkstyle.apply_from_file(style.attrib['fromfile'])
        else:
            self.tkstyle.apply_from_string(style.text)

    # =========================================================================
    # Visitor Entry Point
    # =========================================================================

    def _parse_tree(self):
        """Parse XML and build interface."""
        for br in self._root.findall("*//br"):
            br.tail = "\n" + br.tail if br.tail else "\n"
        if self._root.tag != 'root':
            msg = 'Invalid root tag! Expecting "root", but found %s'
            print(msg % self._root.tag, file=sys.stderr)
            return False
        self._handle_container(self._root, self._master)
        return True

    # =========================================================================
    # Public API
    # =========================================================================

    def add_tag(self, tag: str, cls, container=False):
        """Maps a tag to a class.
        
            :param tag:
                XML tag name
            :type tag:
                str
            :param cls:
                Class to be instantiated when *tag* is found
            :type cls:
                Any widget class
            :param container:
                Whether this Tk widget is a container of other widgets
        """
        self._tag2tk[tag] = cls
        if container:
            self._containers.append(tag)

    def set_custom_child_tag(self, tag: str, child_tag: str, handler):
        """Set a handler for a custom (user-defined) child tag.

            :param tag:
                XML tag name.
            :type tag:
                str
            :param child_tag:
                The XML tag to be registered for this widget.
            :type attribute:
                str
            :param handler:
                Handler function with signature f(widget, child_node).
        """
        self._special_children[(tag, child_tag)] = handler

    def set_custom_attribute(self, tag: str, attribute: str, handler):
        """Set a handler for a custom (user-defined) attribute.

            :param tag:
                XML tag name.
            :type tag:
                str
            :param attribute:
                XML attribute name.
            :type attribute:
                str
            :param handler:
                Handler function with signature f(widget, key, strval).
        """
        self._special_attribs[(tag, attribute)] = handler

    def build_from_file(self, filepath):
        """Build user interface from XML file."""
        self._tree = ElementTree.parse(filepath)
        self._root = self._tree.getroot()
        self._parse_tree()

    def build_from_string(self, contents):
        """Build user interface from XML string"""
        self._root = ElementTree.fromstring(contents)
        self._parse_tree()

    def build_from_asset(self, asset_path):
        """Build user interface from XML filepath or resource.
        
        Resources are specified like an URI: (e.g.: `res://package/resource`).
        File paths are specified either with the standard platform conventions
        or prefixed with the protocol `file://`. 
        Currently, other protocols are not supported.
        """
        content = helpers.read_asset(asset_path)
        self.build_from_string(content.decode('utf8'))

    def populate_namespace(self, target):
        """Populates namespaces from nodes WIDs and variable names.
        
        This function will add variables to an object according to the
        UI XML and some naming convention. For example, a `Label` having
        the WID label-name will be mapped to an attribute _label_name
        into the target object, and a variable called "age" will be mapped
        to _var_age.

        :param target:
                Python object to add attributes.
        """
        for wid, widget in self.nodes.items():
            setattr(target, '_' + wid.replace('-', '_'), widget)
        for name, variable in self.vars.items():
            setattr(target, '_var_' + name.replace('-', '_'), variable)


if __name__ == '__main__':
    import doctest
    doctest.testmod()