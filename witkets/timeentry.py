#!/usr/bin/env python3

import tkinter as tk
import tkinter.ttk as ttk
from witkets.spin import Spin
from witkets.core import wtk_types
import datetime as dt


class TimeEntry(ttk.Frame):
    """Entry for inputting time (hours, minutes and seconds)
    
    Convenient widget that combines 3 spins and 2 labels to compose an entry
    suitable for inputting time.
    
    Options:
       - seconds --- Whether to show the seconds spin (constructor only)
       - All :code:`Frame` widget options
    """

    def __init__(self, master=None, seconds=True, **kw):
        ttk.Frame.__init__(self, master, **kw)
        self.spin_hours = Spin(self, to=23, orientation=tk.VERTICAL)
        self.spin_hours['numberformat'] = '%02d'
        self.spin_hours['circular'] = True
        self.spin_hours.entry['width'] = 2
        self.spin_hours.pack(side=tk.LEFT)
        self.label1 = ttk.Label(self, text=':')
        self.label1.pack(side=tk.LEFT)
        self.spin_minutes = Spin(self, to=59, orientation=tk.VERTICAL)
        self.spin_minutes['numberformat'] = '%02d'
        self.spin_minutes['circular'] = True
        self.spin_minutes.entry['width'] = 2
        self.spin_minutes.pack(side=tk.LEFT)
        self.label2 = ttk.Label(self, text=':')
        self.spin_seconds = Spin(self, to=59, orientation=tk.VERTICAL)
        self.spin_seconds['numberformat'] = '%02d'
        self.spin_seconds['circular'] = True
        self.spin_seconds.entry['width'] = 2
        if seconds:
            self.label2.pack(side=tk.LEFT)
            self.spin_seconds.pack(side=tk.LEFT)
        self._monitoring = True
        self.spin_hours['variable'].trace_add('write', self._on_spin_changed) 
        self.spin_minutes['variable'].trace_add('write', self._on_spin_changed) 
        self.spin_seconds['variable'].trace_add('write', self._on_spin_changed) 

    # =====================================================================            
    # Introspection
    # =====================================================================

    widget_keys = {
        'circular': bool,
        'state': wtk_types.state
    }

    # =====================================================================            
    # Custom Events
    # =====================================================================

    def _on_spin_changed(self, *args):
        if self._monitoring:
            self.event_generate('<<Changed>>')

    # =====================================================================
    # Config methods
    # =====================================================================

    def _handle_witket_key(self, key, val):
        if key == 'circular':
            self.spin_hours['circular'] = val
            self.spin_minutes['circular'] = val
            self.spin_seconds['circular'] = val
        elif key == 'state':
            self.spin_hours['state'] = val
            self.spin_minutes['state'] = val
            self.spin_seconds['state'] = val
        elif key == 'seconds':
            raise Exception('[Trying to change constructor-only property (TimeEntry -> seconds)!')

    def __setitem__(self, key, val):
        if key in Spin.widget_keys:
            self._handle_witket_key(key, val)
        else:
            ttk.Frame.__setitem__(self, key, val)

    def __getitem__(self, key):
        if key == 'circular':
            return self.spin_hours['circular']
        elif key == 'state':
            return self.spin_hours['state']
        else:
            return ttk.Frame.__getitem__(self, key)

    def config(self, **kw):
        """Standard Tk config method"""
        needle = kw.copy()
        for key, val in needle.items():
            if TimeEntry.widget_keys:
                self._handle_witket_key(key, val)
                kw.pop(key, False)
        ttk.Frame.config(self, **kw)

    # =====================================================================            
    # Public API
    # =====================================================================

    def get(self):
        """Gets the time stored in the widget as a `datetime.time` object """
        hours = int(self.spin_hours.get())
        minutes = int(self.spin_minutes.get())
        seconds = int(self.spin_seconds.get())
        return dt.time(hours, minutes, seconds)

    def set(self, time: dt.time):
        """Sets the time stored in the widget
        
        This method won't fire the <<Changed>> event.
        """
        self._monitoring = False
        self.spin_hours.set(time.hour)
        self.spin_minutes.set(time.minute)
        self.spin_seconds.set(time.second)
        self._monitoring = True


if __name__ == '__main__':
    root = tk.Tk()
    time_entry = TimeEntry(root)
    time_entry.pack(expand=0)
    time_entry.set(23, 48, 36)
    print(time_entry.get())
    root.mainloop()
