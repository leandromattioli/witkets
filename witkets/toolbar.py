import tkinter as tk
import tkinter.ttk as ttk
from witkets.core.helpers import read_asset

class Toolbar(ttk.Frame):
    """Simple toolbar implementation
      
       Options: All :code:`Frame` widget options
    """

    def __init__(self, master=None, orientation=tk.HORIZONTAL, **kw):
        ttk.Frame.__init__(self, master, **kw)
        self._button_count = 0
        self._orientation = orientation
        self['style'] = 'Toolbar.TFrame'

    def add_button(self, imagepath, command=None, text=None, name=None):
        """Adds a button to the toolbar.

        The button will have a default style of Toolbar.TButton
        
        Returns:
            The Button added.
        """
        content = read_asset(imagepath)
        img = tk.PhotoImage(data=content)
        kw = {'image': img }
        if text:
            kw['text'] = text
            kw['compound'] = tk.TOP if self._orientation == tk.HORIZONTAL else tk.LEFT
        if command:
            kw['command'] = command
        btn = ttk.Button(self, **kw)  # both img needed
        btn.image = img # this is needed to keep a reference to the image alive
        btn.pack(side=tk.LEFT if self._orientation == tk.HORIZONTAL else tk.TOP, padx=2, pady=2, 
                 fill=tk.X if self._orientation == tk.HORIZONTAL else tk.Y, expand=0)
        btn['style'] = 'Toolbar.TButton'
        btn.name = name if name else f'button-{self._button_count}'
        self._button_count += 1
        return btn
    
    def get_button_by_name(self, name):
        """Get a Button child by its name"""
        for btn in self.winfo_children():
            if btn.name == name:
                return btn
        return None


if __name__ == '__main__':
    def hello():
        print('hello!!')


    icon1 = '/usr/share/icons/gnome/32x32/actions/add.png'
    icon2 = '/usr/share/icons/gnome/32x32/status/dialog-error.png'

    root = tk.Tk()

    toolbar = Toolbar()
    toolbar.add_button(icon1, hello)
    toolbar.add_button(icon2, hello, text='Test')
    toolbar.pack()

    root.mainloop()
