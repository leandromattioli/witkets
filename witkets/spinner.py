#!/usr/bin/env python3

import tkinter as tk
import tkinter.ttk as ttk
from math import pi, sin, cos


class Spinner(tk.Canvas):
    """Simple spinner for indicating long actions
        
       Options:
          - active --- Whether the spinner is active
          - timestep --- Timeout to render next animation frame (in milliseconds)
          - All :code:`Canvas` widget options (notably width and height)

    """

    def __init__(self, master=None, active=False, timestep=100, **kw):
        self._widget_keys = ('active', 'timestep')
        self._active = active
        self._timestep = timestep
        # Canvas
        if 'highlightthickness' not in kw:
            kw['highlightthickness'] = 0
        if 'width' not in kw:
            kw['width'] = 25
        if 'height' not in kw:
            kw['height'] = 25
        tk.Canvas.__init__(self, master, **kw)
        self.bind("<Configure>", self._on_resize)
        # Canvas objects, math-related vars and timer references
        self._circles = []
        self._curr_angle = 0  # 0 to 7 ; 45 degree step
        self._timer = None
        # Highlight thickness
        self._draw()

    def __setitem__(self, key, val):
        if key in self._widget_keys:
            setattr(self, '_' + key, val)
        else:
            tk.Canvas.__setitem__(self, key, val)
        self._redraw()

    def __getitem__(self, key):
        if key in self._widget_keys:
            return getattr(self, '_' + key)
        else:
            return tk.Canvas.__getitem__(self, key)

    def config(self, **kw):
        """Standard Tk config method"""
        base_kw = {}
        for key in kw:
            if key in self._widget_keys:
                setattr(self, '_' + key, kw[key])
            else:
                base_kw[key] = kw[key]
        tk.Canvas.config(self, **base_kw)
        self._redraw()

    def _on_resize(self, event):
        self._redraw()

    def _redraw(self):
        """Redraws the Spinner widget"""
        if self._timer is not None:
            self.after_cancel(self._timer)
        if self._circles:
            for c in self._circles:
                self.delete(c)
        self._draw()

    def _draw(self):
        w = self.winfo_width()
        h = self.winfo_height()
        xc, yc = w / 2, h / 2
        small_r = 0.1 * w
        big_r = w / 2 - small_r - 2
        f = self._curr_angle * 25
        for i in range(8):
            x = xc + big_r * cos(pi / 4 * i)
            y = yc + big_r * sin(pi / 4 * i)
            r = (x - small_r, y - small_r, x + small_r, y + small_r)
            f = (f + 25) % 200
            color = '#' + hex(f)[2:] * 3
            self._circles.append(self.create_oval(fill=color, outline="", *r))
        self._curr_angle += 1
        if self._active:
            self._timer = self.after(self._timestep, self._draw)


if __name__ == '__main__':
    def toggle():
        spinner['active'] = not spinner['active']

    root = tk.Tk()
    spinner = Spinner(root, width=100, height=100, timestep=200)
    spinner['active'] = True
    spinner.pack(expand=1, fill='both')
    button = tk.Button(root, text='Toggle active')
    button.pack()
    button['command'] = toggle
    root.mainloop()
