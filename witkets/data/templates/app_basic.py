import tkinter as tk
import tkinter.ttk as ttk
import witkets as wtk
from tkinter.messagebox import showinfo

def on_button1_clicked():
    name = entry.get().strip()
    if name:
        showinfo(title='Thanks!', message='Hello, %s!' % name)

if __name__ == '__main__':
    root = tk.Tk()
    root.title('@APP_NAME@')
    builder = wtk.TkBuilder(root)
    builder.build_from_file('gui.xml')
    entry = builder.nodes['entry1']
    button = builder.nodes['btn1']
    button['command'] = on_button1_clicked
    root.mainloop()








