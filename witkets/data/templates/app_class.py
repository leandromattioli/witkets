import tkinter as tk
import tkinter.ttk as ttk
import witkets as wtk
from tkinter.messagebox import showinfo

class Application(ttk.Frame):
    """Main GUI Class"""

    def __init__(self, master=None, **kwargs):
        ttk.Frame.__init__(self, master, **kwargs)
        self.builder = wtk.TkBuilder(self)
        self.builder.build_from_file('gui.xml')
        self._config_widgets()

    def _config_widgets(self):
        self.entry = self.builder.nodes['entry1']
        button = self.builder.nodes['btn1']
        button['command'] = self.on_button1_clicked

    def on_button1_clicked(self):
        name = self.entry.get().strip()
        if name:
            showinfo(title='Thanks!', message='Hello, %s!' % name)


if __name__ == '__main__':
    root = tk.Tk()
    root.title('@APP_NAME@')
    app = Application(root)
    app.pack()
    root.mainloop()

