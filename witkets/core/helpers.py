from importlib import resources

def read_resource(res_uri):
    """Reads binary content from a resource URI-like string.

    The default mechanism is to return `importlib.resources.read_binary(package, resource)`.
    This behaviour can be overridden by the user just by redefining the 
    `witkets.core.helpers.read_resource` symbol itself.

    Parameters:
        res_uri (string): Path in the format `res://package/resource`.
    """
    try:
        package, resource = res_uri.replace('res://', '').split('/', 1)
    except:
        raise ValueError(f'Malformed resource: {res_uri}')
    return resources.read_binary(package, resource)


def read_asset(asset_path):
    """Read an asset from a file or resource path.
    
    The file may or not be prefixed with the protocol `file://`.
    Resources MUST explicitly indicates the protocol `res://`
    """
    if asset_path.startswith('res://'):
        return read_resource(asset_path)
    else:
        simple_path = asset_path.replace('file://', '')
        with open(simple_path, 'rb') as fd:
            contents = fd.read()
        return contents
